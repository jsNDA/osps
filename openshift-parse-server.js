const cluster = require('cluster'),
    stopSignals = [
        'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
        'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
    ],
    production = process.env.NODE_ENV == 'production';

var stopping = false;

cluster.on('disconnect', function (worker) {
    if (production) {
        if (!stopping) {
            cluster.fork();
        }
    } else {
        process.exit(1);
    }
});


if (cluster.isMaster) {
    const workerCount = process.env.NODE_CLUSTER_WORKERS || 4;
    var i;
    for (i = 0; i < workerCount; i += 1) {
        cluster.fork();
    }
    console.log(['Starting', workerCount, 'workers...'].join(' '));
    if (production) {
        stopSignals.forEach(function (signal) {
            process.on(signal, function () {
                console.log(['Got', signal, 'stopping workers...'].join(' '));
                stopping = true;
                cluster.disconnect(function () {
                    console.log('All workers stopped, exiting.');
                    process.exit(0);
                });
            });
        });
    }
} else {

    // Example express application adding the parse-server module to expose Parse
    // compatible API routes.

    var express = require('express');
    var ParseServer = require('parse-server').ParseServer;
    var path = require('path');

    var databaseUri =
        process.env.DATABASE_URI ||
        process.env.MONGODB_URI ||
        process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME;

    if (!databaseUri) {
        console.log('DATABASE_URI not specified, falling back to localhost.');
    }

    var api = new ParseServer({
        databaseURI: databaseUri || 'mongodb://localhost:27017/dev',
        cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
        appId: process.env.APP_ID || 'myAppId',
        masterKey: process.env.MASTER_KEY || '', //Add your master key here. Keep it secret!
        serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
        liveQuery: {
            classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
        }
    });
    // Client-keys like the javascript key or the .NET key are not necessary with parse-server
    // If you wish you require them, you can set them as options in the initialization above:
    // javascriptKey, restAPIKey, dotNetKey, clientKey

    var app = express();

    // Serve static assets from the /public folder
    app.use('/public', express.static(path.join(__dirname, '/public')));

    // Serve the Parse API on the /parse URL prefix
    var mountPath = process.env.PARSE_MOUNT || '/parse';
    app.use(mountPath, api);

    // Parse Server plays nicely with the rest of your web routes
    app.get('/', function (req, res) {
        res.status(200).send('Make sure to star the parse-server repo on GitHub!');
    });

    // There will be a test page available on the /test path of your server url
    // Remove this before launching your app
    app.get('/test', function (req, res) {
        res.sendFile(path.join(__dirname, '/public/test.html'));
    });

    var port = process.env.PORT || 1337;
    var httpServer = require('http').createServer(app);
    httpServer.listen(port, process.env.NODE_IP || 'localhost', function () {
        console.log('parse-server-example running on port ' + port + '.');
    });

// This will enable the Live Query real-time server
    ParseServer.createLiveQueryServer(httpServer);
}